/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "app_commands.h"
#include "parson.h"
#include "config.h"
#include <cyassl/ssl.h>
#include <tcpip/src/hash_fnv.h>

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************
#define TOKEN "48ac93f1e3bb83747f38d581"
// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/

int32_t _APP_ParseUrl(char *uri, char **host, char **path, uint16_t * port);

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    memset(&appData, 0, sizeof(appData));
    /* Place the App state machine in its initial state. */
//    appData.host = "iot.cariboutech.com";
//    strcpy(appData.urlBuffer, "https://159.203.55.85");
    strcpy(appData.urlBuffer, "https://192.241.186.56");
//    appData.host = "192.241.186.56";
    appData.port = 443;
    appData.state = APP_STATE_INIT;
    APP_Commands_Init();
    appData.ipMode = 4;
    appData.cyaSSLLogEnabled = 0;
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
char networkBuffer[256];
char buffer2[1024];
char buffer3[1024];

TCPIP_NET_HANDLE    netH;
int                 i, nNets;
const uint8_t *MacAddr;
 uint8_t nullMacAddr[6];

void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            appData.state = APP_TCPIP_WAIT_FOR_TCPIP_INIT;
                SYS_CONSOLE_MESSAGE(" APP: Initialization\r\n");
            break;
        }

        case APP_TCPIP_WAIT_FOR_TCPIP_INIT:
        {
            SYS_STATUS tcpipStat;
            tcpipStat = TCPIP_STACK_Status(sysObj.tcpip);
            const char          *netName, *netBiosName;
            int                 i, nNets;
            TCPIP_NET_HANDLE    netH;
            if(tcpipStat < 0)
            {   // some error occurred
                SYS_CONSOLE_MESSAGE(" APP: TCP/IP stack initialization failed!\r\n");
                appData.state = APP_TCPIP_ERROR;
            }
            else if(tcpipStat == SYS_STATUS_READY)
            {
                // now that the stack is ready we can check the
                // available interfaces
                nNets = TCPIP_STACK_NumberOfNetworksGet();
                for(i = 0; i < nNets; i++)
                {

                    netH = TCPIP_STACK_IndexToNet(i);
                    netName = TCPIP_STACK_NetNameGet(netH);
                    netBiosName = TCPIP_STACK_NetBIOSName(netH);

#if defined(TCPIP_STACK_USE_NBNS)
                    SYS_CONSOLE_PRINT("    Interface %s on host %s - NBNS enabled\r\n", netName, netBiosName);
#else
                    SYS_CONSOLE_PRINT("    Interface %s on host %s - NBNS disabled\r\n", netName, netBiosName);
#endif  // defined(TCPIP_STACK_USE_NBNS)

                }
                appData.state = APP_TCPIP_WAIT_FOR_IP;

            }
            break;
        }
        case APP_TCPIP_WAIT_FOR_IP:
        {
            int                 i, nNets;
            TCPIP_NET_HANDLE    netH;
            IPV4_ADDR           ipAddr;
            // if the IP address of an interface has changed
            // display the new value on the system console
            nNets = TCPIP_STACK_NumberOfNetworksGet();

            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                ipAddr.Val = TCPIP_STACK_NetAddress(netH);
                if(0 != ipAddr.Val)
                {
                    SYS_CONSOLE_MESSAGE(TCPIP_STACK_NetNameGet(netH));
                    SYS_CONSOLE_MESSAGE(" IP Address: ");
                    SYS_CONSOLE_PRINT("%d.%d.%d.%d \r\n", ipAddr.v[0], ipAddr.v[1], ipAddr.v[2], ipAddr.v[3]);
                    if (ipAddr.v[0] != 0 && ipAddr.v[0] != 169) // Wait for a Valid IP
                    {
 //                       appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                        appData.state = APP_TCPIP_PROCESS_COMMAND;
                        SYS_CONSOLE_MESSAGE("Waiting for command type: openurl <url>\r\n");
                    }
                }
            }
            break;
        }
        case APP_TCPIP_WAITING_FOR_COMMAND:
            SYS_CMD_READY_TO_READ();
            break;
                    
            
        case APP_TCPIP_PROCESS_COMMAND:
        {
            if (_APP_ParseUrl(appData.urlBuffer, &appData.host, &appData.path, &appData.port))
            {
                SYS_CONSOLE_PRINT("Could not parse URL '%s'\r\n", appData.urlBuffer);
                appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                break;
            }
            appData.testStart = SYS_TMR_SystemCountGet();
            appData.dnsComplete = 0;
            appData.connectionOpened = 0;
            appData.sslNegComplete = 0;
            appData.firstRxDataPacket = 0;
            appData.lastRxDataPacket = 0;
            appData.rawBytesReceived = 0;
            appData.rawBytesSent = 0;
            appData.clearBytesReceived = 0;
            appData.clearBytesSent = 0;
            //appData.cyaSSLLogEnabled = 1; // Uncomment this if you want cyassl debugging output.  Make sure its selected in MHC too


            /* First check to see if host is an IPv4 or IPv6 address*/
            if (TCPIP_Helper_StringToIPAddress(appData.host, &appData.address.v4Add))
            {
                appData.queryState = 4;
                if (appData.ipMode != 0 && appData.ipMode != 4)
                {
                    SYS_CONSOLE_PRINT("Inputed URL is an IPv4 Address but ipmode is set to %d\r\n", appData.ipMode);
                    appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                    break;
                }
                else
                {
                    SYS_CONSOLE_PRINT("Using IPv4 Address: %d.%d.%d.%d for host '%s'\r\n",
                            appData.address.v4Add.v[0],
                            appData.address.v4Add.v[1],
                            appData.address.v4Add.v[2],
                            appData.address.v4Add.v[3],
                            appData.host);
                }
            }
            else if (TCPIP_Helper_StringToIPv6Address(appData.host, &appData.address.v6Add))
            {
                appData.queryState = 6;
                if (appData.ipMode != 0 && appData.ipMode != 6)
                {
                    SYS_CONSOLE_PRINT("Inputed URL is an IPv6 Address but ipmode is set to %d\r\n", appData.ipMode);
                    appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                    break;
                }
                else
                {
                    SYS_CONSOLE_PRINT("Using IPv6 Address: %x:%x:%x:%x:%x:%x:%x:%x for host '%s'\r\n",
                            appData.address.v6Add.w[0],
                            appData.address.v6Add.w[1],
                            appData.address.v6Add.w[2],
                            appData.address.v6Add.w[3],
                            appData.address.v6Add.w[4],
                            appData.address.v6Add.w[5],
                            appData.address.v6Add.w[6],
                            appData.address.v6Add.w[7],
                            appData.host);
                }
            }
            else
            {
                TCPIP_DNS_RESULT result;
                SYS_CONSOLE_PRINT("Using DNS to Resolve '%s'\r\n", appData.host);
                if (appData.ipMode == 0 || appData.ipMode == 6)
                {
                    result = TCPIP_DNS_Resolve(appData.host, TCPIP_DNS_TYPE_AAAA);
                    appData.queryState = 6;
                }
                else
                {
                    result = TCPIP_DNS_Resolve((const char *)appData.host, TCPIP_DNS_TYPE_A);
                    appData.queryState = 4;
                }
                SYS_ASSERT(result != TCPIP_DNS_RES_NAME_IS_IPADDRESS, "DNS Result is TCPIP_DNS_RES_NAME_IS_IPADDRESS, which should not happen since we already checked");
                if(result >= 0)
                {
                    appData.state = APP_TCPIP_WAIT_ON_DNS;
                }
                else
                {
                    SYS_CONSOLE_PRINT("DNS Query returned %d Aborting\r\n", result);
                    appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                }
                break;
            }
            // If we're here, we have a valid IP address
            appData.state = APP_TCPIP_START_CONNECTION;
            break;
        }
                
        case APP_TCPIP_WAIT_ON_DNS:
        {
 //           TCPIP_DNS_RESULT result = TCPIP_DNS_IsResolved((const char *)appData.host, &appData.address, appData.ipMode == 4 ? IP_ADDRESS_TYPE_IPV4 : IP_ADDRESS_TYPE_IPV6);
            TCPIP_DNS_RESULT result = TCPIP_DNS_IsResolved((const char *)appData.host, &appData.address, IP_ADDRESS_TYPE_IPV4);
            switch (result)
            {
                case TCPIP_DNS_RES_PENDING:
                {
                }
                break;
                case TCPIP_DNS_RES_OK:
                {
                    if (appData.queryState == 4)
                    {
                        SYS_CONSOLE_PRINT("DNS Resolved IPv4 Address: %d.%d.%d.%d for host '%s'\r\n",
                            appData.address.v4Add.v[0],
                            appData.address.v4Add.v[1],
                            appData.address.v4Add.v[2],
                            appData.address.v4Add.v[3],
                            appData.host);
                    }
                    else
                    {
                        SYS_CONSOLE_PRINT("DNS Resolved IPv6 Address: %x:%x:%x:%x:%x:%x:%x:%x for host '%s'\r\n",
                            htons(appData.address.v6Add.w[0]),
                            htons(appData.address.v6Add.w[1]),
                            htons(appData.address.v6Add.w[2]),
                            htons(appData.address.v6Add.w[3]),
                            htons(appData.address.v6Add.w[4]),
                            htons(appData.address.v6Add.w[5]),
                            htons(appData.address.v6Add.w[6]),
                            htons(appData.address.v6Add.w[7]),
                            appData.host);
                    }
                    appData.state = APP_TCPIP_START_CONNECTION;
                    break;
                }
                default:
                {
                    if (appData.queryState == 4 || appData.ipMode == 6)
                    {
                        SYS_CONSOLE_PRINT("DNS Is Resolved returned %d Aborting\r\n", result);
                        appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                        break;
                    }
                    else
                    {
                        SYS_CONSOLE_PRINT("DNS Is Resolved returned %d trying IPv4 Address\r\n", result);
                        result = TCPIP_DNS_Resolve(appData.host, TCPIP_DNS_TYPE_A);
                        appData.queryState = 4;
                        if(result >= 0)
                        {
                            appData.state = APP_TCPIP_WAIT_ON_DNS;
                        }
                        else
                        {
                            SYS_CONSOLE_PRINT("DNS Query returned %d Aborting\r\n", result);
                            appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
                        }
                    }
                }
            }
        }
        break;
        case APP_TCPIP_START_CONNECTION:
        {
            // If we're here it means that we have a proper address.
            appData.dnsComplete = SYS_TMR_SystemCountGet();
            if (appData.queryState == 4)
            {
                SYS_CONSOLE_PRINT("Starting TCP/IPv4 Connection to : %d.%d.%d.%d port  '%d'\r\n",
                            appData.address.v4Add.v[0],
                            appData.address.v4Add.v[1],
                            appData.address.v4Add.v[2],
                            appData.address.v4Add.v[3],
                            appData.port);
                appData.socket = NET_PRES_SocketOpen(0, 
                            NET_PRES_SKT_UNENCRYPTED_STREAM_CLIENT, 
                            IP_ADDRESS_TYPE_IPV4, 
                            appData.port, 
                            (NET_PRES_ADDRESS *)&appData.address,
                            NULL);
            }
            else
            {
                SYS_CONSOLE_PRINT("Starting TCP/IPv6 Address: %x:%x:%x:%x:%x:%x:%x:%x port '%d'\r\n",
                            htons(appData.address.v6Add.w[0]),
                            htons(appData.address.v6Add.w[1]),
                            htons(appData.address.v6Add.w[2]),
                            htons(appData.address.v6Add.w[3]),
                            htons(appData.address.v6Add.w[4]),
                            htons(appData.address.v6Add.w[5]),
                            htons(appData.address.v6Add.w[6]),
                            htons(appData.address.v6Add.w[7]),
                            appData.port);
                appData.socket = NET_PRES_SocketOpen(0, 
                                                   NET_PRES_SKT_UNENCRYPTED_STREAM_CLIENT,
                                                   IP_ADDRESS_TYPE_IPV6,
                                                   appData.port,
                                                   (NET_PRES_ADDRESS *)&appData.address,
                                                   NULL);
            }
            NET_PRES_SocketWasReset(appData.socket);
            if (appData.socket == INVALID_SOCKET)
            {
                SYS_CONSOLE_MESSAGE("Could not create socket - aborting\r\n");
                appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
            }
            //SYS_CONSOLE_MESSAGE("Starting connection\r\n");
            appData.state = APP_TCPIP_WAIT_FOR_CONNECTION;
        }
        case APP_TCPIP_WAIT_FOR_CONNECTION:
        {
            if (!NET_PRES_SocketIsConnected(appData.socket))
            {
                break;
            }
            appData.connectionOpened = SYS_TMR_SystemCountGet();
            if (appData.urlBuffer[4] == 's')
            {
                SYS_CONSOLE_MESSAGE("Connection Opened: Starting SSL Negotiation\r\n");
                if (!NET_PRES_SocketEncryptSocket(appData.socket))
                {
                    SYS_CONSOLE_MESSAGE("SSL Create Connection Failed - Aborting\r\n");
                    appData.state = APP_TCPIP_CLOSE_CONNECTION;
                }
                else
                {
                    appData.state = APP_TCPIP_WAIT_FOR_SSL_CONNECT;

                }
            }
            else
            {
                SYS_CONSOLE_MESSAGE("Connection Opened: Starting Clear Text Communication\r\n");
                appData.state = APP_TCPIP_SEND_REQUEST;
            }
            break;
        }
        
        case APP_TCPIP_SEND_REQUEST:
        {
            if(NET_PRES_SocketWriteIsReady(appData.socket, sizeof(networkBuffer), sizeof(networkBuffer)) == 0)
            {
                break;
            }
            sprintf(networkBuffer, "GET /%s HTTP/1.1\r\n"
                    "Host: %s\r\n"
                    "Connection: close\r\n\r\n", appData.path, appData.host);
            NET_PRES_SocketWrite(appData.socket, (uint8_t*)networkBuffer, strlen(networkBuffer));
            appData.clearBytesSent += strlen(networkBuffer);
            appData.rawBytesSent += strlen(networkBuffer);
            appData.state = APP_TCPIP_WAIT_FOR_RESPONSE;
            break;
        }
        
        case APP_TCPIP_WAIT_FOR_RESPONSE:
        {
            if (NET_PRES_SocketReadIsReady(appData.socket) == 0)
            {
                if (NET_PRES_SocketWasReset(appData.socket))
                {
                    appData.state = APP_TCPIP_CLOSE_CONNECTION;
                }
                break;
            }
            if (appData.firstRxDataPacket == 0)
            {
                appData.firstRxDataPacket = SYS_TMR_SystemCountGet();
            }
            appData.lastRxDataPacket = SYS_TMR_SystemCountGet();
            uint16_t res = NET_PRES_SocketRead(appData.socket, (uint8_t*)networkBuffer, sizeof(networkBuffer));
            appData.clearBytesReceived += res;
            appData.rawBytesReceived += res;
            break;
        }
        case APP_TCPIP_WAIT_FOR_SSL_CONNECT:
        {
            if (NET_PRES_SocketIsNegotiatingEncryption(appData.socket))
            {
                break;
            }
            if (!NET_PRES_SocketIsSecure(appData.socket))
            {
                SYS_CONSOLE_MESSAGE("SSL Connection Negotiation Failed - Aborting\r\n");
                appData.state = APP_TCPIP_CLOSE_CONNECTION;
                break;
            }
            SYS_CONSOLE_MESSAGE("SSL Connection Opened: Starting Clear Text Communication\r\n");
            appData.cyaSSLLogEnabled = 0;
            appData.sslNegComplete = SYS_TMR_SystemCountGet();;
//            appData.state = APP_TCPIP_SEND_REQUEST_SSL;
            appData.state = APP_REGISTER_USER;
//            appData.state = APP_TCPIP_REQUEST_SSL;
            break;
        }
        
        case APP_REGISTER_USER:
        {
            int ret;
            memset(buffer2, 0, sizeof(buffer2));
            if (!NET_PRES_SocketIsConnected(appData.socket))
            {
                break;
            }                                              
            nNets = TCPIP_STACK_NumberOfNetworksGet();
            for (i = 0; i < nNets; i++)
            {
                netH = TCPIP_STACK_IndexToNet(i);
                MacAddr = TCPIP_STACK_NetAddressMac(netH);
            }
            for (i=0; i<6; i++)
            {
                nullMacAddr[i] = *MacAddr;
                MacAddr++;
            }

            snprintf(appData.MacAddress, sizeof(appData.MacAddress), "%02x:%02x:%02x:%02x:%02x:%02x", nullMacAddr[0],
                    nullMacAddr[1],nullMacAddr[2],nullMacAddr[3],nullMacAddr[4],nullMacAddr[5]);
            
            appData.path="/v1/iot_service/devices";
            
            sprintf(buffer2, "PUT %s HTTP/1.1\r\n"
                    "Host: %s:443\r\n"
                    "Content-Type:application/json\r\n"
//                    "Connection: close\r\n"
                    "Content-Length:29\r\n"
                    "provision-token:%s\r\n"
                    "\r\n"
                     "{\"macId\":\"%s\"}", appData.path, appData.host, TOKEN, appData.MacAddress);
                                
            ret = NET_PRES_SocketWrite(appData.socket, (uint8_t*)buffer2, strlen(buffer2));
            appData.clearBytesSent += ret;
            appData.state = APP_REGISTRATION_RESPONSE;
            break;
        }
        
        case APP_REGISTRATION_RESPONSE:
        {
            
            memset(buffer3, 0, sizeof(buffer3));
            if (!NET_PRES_SocketIsConnected(appData.socket))
            {
                break;
            }
            if (NET_PRES_SocketReadIsReady(appData.socket))
            {
                NET_PRES_SocketRead(appData.socket, (uint8_t*)buffer3, sizeof(buffer3));
                if(buffer3[156] == '\0')
                {
                    appData.state = APP_REGISTRATION_RESPONSE;
                }
                else
                    appData.state = APP_TCPIP_IDLE;                
            }
         break;   
        }
        
//        case APP_TCPIP_REQUEST_SSL:
//        {
//            appData.path="/v1/auth-service/app-details";
//            sprintf(networkBuffer, "GET %s HTTP/1.1\r\n"
//                    "Host: %s:443\r\n"
//                    "Content-Type:application/json\r\n"
//                    "Connection: close\r\n\r\n", appData.path, appData.host);
//            int ret;
//            ret = NET_PRES_SocketWrite(appData.socket, (uint8_t*)networkBuffer, strlen(networkBuffer));
//            appData.clearBytesSent += ret;
//            appData.state = APP_TCPIP_RESPONSE;
//            break;
//        }
//        
//        case APP_TCPIP_RESPONSE:
//        {
//            memset(buffer3, 0, sizeof(buffer3));
//            if (!NET_PRES_SocketIsConnected(appData.socket))
//            {
//                break;
//            }
//            if (NET_PRES_SocketReadIsReady(appData.socket))
//            {
//                NET_PRES_SocketRead(appData.socket, (uint8_t*)buffer3, sizeof(buffer3));
//                if(buffer3[3] == '\0')
//                {
//                    appData.state = APP_TCPIP_RESPONSE;
//                }
//                else
//                    appData.state = APP_TCPIP_IDLE;                
//            }
//         break;   
//        }
        
        case APP_TCPIP_IDLE:
        {
            break;
        }
//        case APP_PARSE_SCOPEID:
//        {
//            char *data_pointer;
//            data_pointer = strchr(buffer3, '{');
//            JSON_Value *root_value = json_parse_string(data_pointer);
//            if(json_value_get_type(root_value) != JSONObject)
//                return -1;
//            JSON_Object * tObject = json_value_get_object(root_value);
//            appData.scopeid1 = json_object_dotget_string(tObject, "scopeId");
//            strcpy(appData.scopeid ,appData.scopeid1);
//            appData.scopeid[25] = '\0';
// //           appData.state = APP_TCPIP_MQTT_SUBSCRIBE;
//            break;
//        }  
        
        case APP_TCPIP_SEND_REQUEST_SSL:
        {
            sprintf(networkBuffer, "GET /%s HTTP/1.1\r\n"
                    "Host: %s\r\n"
                    "Connection: close\r\n\r\n", appData.path, appData.host);
            int ret;
            ret = NET_PRES_SocketWrite(appData.socket, (uint8_t*)networkBuffer, strlen(networkBuffer));
            appData.clearBytesSent += ret;
            appData.state = APP_TCPIP_WAIT_FOR_RESPONSE_SSL;
            break;
        }
        case APP_TCPIP_WAIT_FOR_RESPONSE_SSL:
        {
            if (NET_PRES_SocketReadIsReady(appData.socket) == 0)
            {
                if (NET_PRES_SocketWasReset(appData.socket))
                {
                    appData.state = APP_TCPIP_CLOSE_CONNECTION;
                }
                break;
            }
            if (appData.firstRxDataPacket == 0)
            {
                appData.firstRxDataPacket = SYS_TMR_SystemCountGet();
            }
            appData.lastRxDataPacket = SYS_TMR_SystemCountGet();
            uint16_t res = NET_PRES_SocketRead(appData.socket, (uint8_t*)networkBuffer, sizeof(networkBuffer));
            appData.clearBytesReceived += res;
            appData.rawBytesReceived += res;
            break;
        }

        case APP_TCPIP_CLOSE_CONNECTION:
        {
            NET_PRES_SocketClose(appData.socket);
            SYS_CONSOLE_MESSAGE("Connection Closed\r\n");
            appData.state = APP_TCPIP_WAITING_FOR_COMMAND;
            break;
        }

        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
 
int32_t _APP_ParseUrl(char *uri, char **host, char **path, uint16_t * port)
{
    char * pos;
    pos = strstr(uri, "://"); //Check to see if its a proper URL
    

    if ( !pos )
    {
        return -1;
    }
    *pos = '\0';

    uint32_t protoHash = fnv_32a_hash(uri, strlen(uri));
    //SYS_CONSOLE_PRINT("Protocol Hash '%s' is 0x%x\r\n", uri, protoHash);

    switch(protoHash)
    {
        case APP_HTTP_FNV:
            *port = 80;
            break;
        case APP_HTTPS_FNV:
            *port = 443;
            break;
    }


    *host = pos + 3; // This is where the host should start
//    pos = *host;
    pos = pos + 2;
    if (**host == '[')
    {
        // IPv6 address
        pos = strchr( *host, ']');
        *host = *host+1;
        *pos = '\0';
        pos++;
    }
    else
    {
        char * pos2 = strchr(pos, ':');
        if (pos2 != NULL)
        {
            pos = pos2;
        }
        else
        {
            pos2 = strchr(pos, '/');
            if (pos2 == NULL)
            {
                return -1;
            }
            pos = pos2;
        }
    }

    if ( *pos != ':' )
    {
        pos = strchr(pos, '/');
        if (!pos)
        {
            *path = NULL;
        }
        else
        {
            *pos = '\0';
            *path = pos + 1;
        }
    }
    else
    {
        *pos = '\0';
        char * portc = pos + 1;

        pos = strchr(portc, '/');
        if (!pos)
        {
            *path = NULL;
        }
        else
        {
            *pos = '\0';
            *path = pos + 1;
        }
        *port = atoi(portc);
    }
    return 0;
}



/*******************************************************************************
 End of File
 */

